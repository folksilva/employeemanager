from __future__ import unicode_literals

from django.db import models

class Employee(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField()
    department = models.CharField(max_length=200)

    def __str__(self):
        return self.name
