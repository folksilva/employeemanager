from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient
from .models import Employee


class EmployeeManagerTests(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.new_employee_data = {'name':'Luiz Silva','email':'luiz@luizalabs.com','department':'Backend'}
        Employee.objects.create(name='Arnaldo Pereira', email='arnaldo@luizalabs.com', department='Architecture')
        Employee.objects.create(name='Renato Pedigoni', email='renato@luizalabs.com', department='E-commerce')
        Employee.objects.create(name='Thiago Catoto', email='catoto@luizalabs.com', department='Mobile')
    
    def test_list_employees(self):
        response = self.client.get('/employee/')
        self.assertEqual(response.data, [
            {'name':'Arnaldo Pereira','email':'arnaldo@luizalabs.com','department':'Architecture'},
            {'name':'Renato Pedigoni','email':'renato@luizalabs.com','department':'E-commerce'},
            {'name':'Thiago Catoto','email':'catoto@luizalabs.com','department':'Mobile'}
        ])
    
    def test_add_employee(self):
        response = self.client.post('/employee/', self.new_employee_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Employee.objects.count(), 4)
        Employee.objects.get(pk=4).delete()
    
    def test_get_employee(self):
        Employee.objects.create(name='Luiz Silva', email='luiz@luizalabs.com', department='Backend')
        response = self.client.get('/employee/4/')
        self.assertEqual(response.data, self.new_employee_data)
        Employee.objects.get(pk=4).delete()
    
    def test_delete_employee(self):
        Employee.objects.create(name='Luiz Silva', email='luiz@luizalabs.com', department='Backend')
        response = self.client.delete('/employee/4/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Employee.objects.count(), 3)
